<?php

namespace App\Console\Commands;

use App\Models\Ounass\Ounass;
use App\Models\User;
use App\Notifications\RMSConsumed;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class ConsumeRMS extends Command
{
    protected $signature = 'consumerms';

    protected $description = 'Consume Data from RMS';

    public function handle()
    {

        // Fixed admin account user //
        $user  = User::where('id', 1)->first();

        // List of files in rms s3 bucket //
        $files = Storage::disk('s3rms')->files('/sit');

        //If files exists
        if (sizeof($files) > 0) {

            // Loop through each file //
            foreach ($files as $input_file) {

                try {
                    // Get last entry count of Ounass table data //
                    $pre_update_count = Ounass::count();

                    // storing current file in consumer disk // 
                    $rms_main_sheet  = $input_file;
                    $s3_file         = Storage::disk('s3rms')->get($rms_main_sheet);
                    $destinationPath = rand(1000000, 9999999) . '.csv';
                    $path            = Storage::disk('consumer')->put($destinationPath, $s3_file);

                    // checking if file naming convension has master keyword //
                    if (strpos($rms_main_sheet, 'master') === false) {

                        // moving master file to consumed folder and deleting source file //
                        $pathCopyFrom = $rms_main_sheet;
                        $pathAr       = explode('/', $pathCopyFrom);
                        $fileName     = end($pathAr);
                        $pathCopyTo   = '/sit/consumed/' . $fileName;
                        Storage::disk('s3rms')->move($pathCopyFrom, $pathCopyTo);
                        $deletepath = public_path('/uploads/' . $destinationPath);
                        \File::delete($deletepath);
                        continue;
                    }

                    // File processing start //
                    $filesAr    = explode('/', $input_file);
                    $file       = end($filesAr);
                    $title      = 'Attempting RMS Import';
                    $newMessage = "A new RMS file will be attempted for import" . "\n" . "FileName: " . $input_file;
                    $message    = ['message' => $newMessage];
                    // Sending new file processing notification //
                    Notification::send($user, new RMSConsumed($message, $title));

                    $fileHandle = fopen(public_path('/uploads/' . $destinationPath), "r");

                    $limit       = 1000;
                    $offset      = 0;
                    $header      = array();
                    $row         = array();
                    $global_i    = 0;
                    $error_count = 0;

                    // Loop through each file records  start//
                    while (!feof($fileHandle)) {
                        //Go to where we were when we ended the last batch
                        fseek($fileHandle, $offset);

                        $i = 0;

                        while (($row = fgetcsv($fileHandle)) !== false) {
                            $i++;
                            $global_i++;

                            if ($global_i == 1) {
                                $header = $row;
                                continue;
                            }

                            if ($row != '' || $row != null) {
                            	$currRow = [];
                                foreach ($header as $k => $key) {
                                    if( !isset($row[$k]) ){
                                        $currRow[trim(strtolower($key))] = '';
                                    }else{
                                        $currRow[trim(strtolower($key))] = $row[$k];
                                    }   
                                }

                                if (!empty($currRow)) {
                                    if (!array_key_exists('channel_name', $currRow) && !array_key_exists('item_idnt', $currRow)) {
                                        DB::table('temp')->insert([
                                            'cause' => json_encode($currRow), 
                                            'reason' => 'channel is null or blank', 
                                            'file' => $rms_main_sheet
                                        ]);
                                        $error_count++;
                                        continue;

                                    } else {

                                        if (!array_key_exists('item_idnt', $currRow) || $currRow['item_idnt'] == 0 || $currRow['item_idnt'] == '' || $currRow['item_idnt'] == null || $currRow['item_idnt'] == '0' || strlen($currRow['item_idnt']) < 5) {
                                            DB::table('temp')->insert([
                                                'cause' => json_encode($currRow), 
                                                'reason' => 'item is null or blank', 
                                                'file' => $rms_main_sheet
                                            ]);
                                            $error_count++;
                                            continue;
                                        }

                                        if (!array_key_exists('item_parent', $currRow) || $currRow['item_parent'] == 0 || $currRow['item_parent'] == '' || $currRow['item_parent'] == null || $currRow['item_parent'] == '0' || strlen($currRow['item_parent']) < 5) {
                                            DB::table('temp')->insert([
                                                'cause' => json_encode($currRow), 
                                                'reason' => 'item parent is null or blank', 
                                                'file' => $rms_main_sheet
                                            ]);
                                            $error_count++;
                                            continue;
                                        }
                                    }

                                    // creating new record in ounass table //
                                    Ounass::updateOrCreate([
                                        'Item' => $currRow['item_idnt'], 'Channel' => $currRow['channel_name'],
                                    ], [
                                        'Channel'               => array_key_exists('channel_name', $currRow) ? $currRow['channel_name'] : null,
                                        'Division'              => array_key_exists('division', $currRow) ? $currRow['division'] : null,
                                        'Division_Name'         => array_key_exists('division_name', $currRow) ? $currRow['division_name'] : null,
                                        'Group_No'              => array_key_exists('group_no', $currRow) ? $currRow['group_no'] : null,
                                        'Group_Name'            => array_key_exists('group_name', $currRow) ? $currRow['group_name'] : null,
                                        'Department'            => array_key_exists('department', $currRow) ? $currRow['department'] : null,
                                        'Department_Name'       => array_key_exists('department_name', $currRow) ? $currRow['department_name'] : null,
                                        'Class'                 => array_key_exists('class', $currRow) ? $currRow['class'] : null,
                                        'Class_Name'            => array_key_exists('class_name', $currRow) ? $currRow['class_name'] : null,
                                        'SubClass'              => array_key_exists('subclass', $currRow) ? $currRow['subclass'] : null,
                                        'SubClass_Name'         => array_key_exists('subclass_name', $currRow) ? $currRow['subclass_name'] : null,
                                        'Gender'                => array_key_exists('gender', $currRow) ? $currRow['gender'] : null,
                                        'Season'                => array_key_exists('season', $currRow) ? $currRow['season'] : null,
                                        'Brand'                 => array_key_exists('brand', $currRow) ? $currRow['brand'] : null,
                                        'Barcode'               => array_key_exists('barcode', $currRow) ? $currRow['barcode'] : null,
                                        'Item'                  => array_key_exists('item_idnt', $currRow) ? $currRow['item_idnt'] : null,
                                        'Item_Parent'           => array_key_exists('item_parent', $currRow) ? $currRow['item_parent'] : null,
                                        'ATG_Color_Code'        => array_key_exists('atg_color_code', $currRow) ? $currRow['atg_color_code'] : null,
                                        'ATG_Color_Desc'        => array_key_exists('atg_color_desc', $currRow) ? $currRow['atg_color_desc'] : null,
                                        'Size'                  => array_key_exists('size', $currRow) ? $currRow['size'] : null,
                                        'Item_Description'      => array_key_exists('item_description', $currRow) ? $currRow['item_description'] : null,
                                        'VPN'                   => array_key_exists('vpn', $currRow) ? $currRow['vpn'] : null,
                                        'GAP_Color_Code'        => array_key_exists('gap_color_code', $currRow) ? $currRow['gap_color_code'] : null,
                                        'import_from'           => $rms_main_sheet,
                                        'is_gender_modified'    => (isset($is_gender_modified) ? $is_gender_modified : null),
                                        'is_gender_modified_by' => (isset($is_gender_modified_by) ? $is_gender_modified_by : null),
                                    ]);
                                }
                            }

                            //If we hit our limit or are at the end of the file
                            if ($i >= $limit) {
                                $offset = ftell($fileHandle);
                                break;
                            }
                        }
                    }
                    // end of Loop //

                    //Close the file
                    fclose($fileHandle);

                    //Update Scripts

                    // Update ounass table according to fixed conditions //
                    Ounass::where('Division_Name', 'Mamas and Papas')->where('Channel', 'Ounass')->update(['Gender' => 'Kids']);
                    Ounass::where('Division_Name', '10 Womens')->where('Channel', 'Ounass')->update(['Gender' => 'Womens']);
                    Ounass::where('Division_Name', '30 Mens')->where('Channel', 'Ounass')->update(['Gender' => 'Mens']);

                    Ounass::where('Division_Name', 'Hanna Andersson')->where('Channel', 'NisNass')->update(['Gender' => 'Kids']);
                    Ounass::where('Division_Name', '65 Kids')->where('Channel', 'NisNass')->update(['Gender' => 'Kids']);
                    Ounass::where('Division_Name', 'Mamas and Papas')->where('Channel', 'NisNass')->update(['Gender' => 'Kids']);
                    Ounass::where('Division_Name', '30 Mens')->where('Channel', 'NisNass')->update(['Gender' => 'Mens']);
                    Ounass::where('Division_Name', '10 Womens')->where('Channel', 'NisNass')->update(['Gender' => 'Womens']);

                    Ounass::where('Group_No', '860')->where('Channel', 'Gap')->where('Department', '8610')->update(['Gender' => 'Mens']);
                    Ounass::where('Group_No', '860')->where('Channel', 'Gap')->where('Department', '8611')->update(['Gender' => 'Womens']);
                    Ounass::where('Group_No', '861')->where('Channel', 'Gap')->where('Department', '8604')->update(['Gender' => 'Kids']);
                    Ounass::where('Group_No', '861')->where('Channel', 'Gap')->where('Department', '8605')->update(['Gender' => 'Kids']);
                    Ounass::where('Group_No', '861')->where('Channel', 'Gap')->where('Department', '8607')->update(['Gender' => 'Kids']);
                    Ounass::where('Group_No', '861')->where('Channel', 'Gap')->where('Department', '8608')->update(['Gender' => 'Kids']);
                    Ounass::where('Group_No', '861')->where('Channel', 'Gap')->where('Department', '8609')->update(['Gender' => 'Kids']);

                    // transfering processed file to consumed folder and deleting source file //
                    $post_update_count = Ounass::count();
                    $pathCopyFrom      = $rms_main_sheet;
                    $pathAr            = explode('/', $pathCopyFrom);
                    $fileName          = end($pathAr);
                    $pathCopyTo        = '/sit/consumed/' . $fileName;
                    Storage::disk('s3rms')->move($pathCopyFrom, $pathCopyTo);
                    //Storage::disk('consumer')->move($pathCopyFrom, $pathCopyTo);

                    $deletepath = public_path('/uploads/' . $destinationPath);
                    \File::delete($deletepath);

                    // Sending new notification for processed file //
                    if (($post_update_count > $pre_update_count) || ($post_update_count == $pre_update_count)) {

                        $updated_count = $post_update_count - $pre_update_count;
                        $title         = 'Aww yeah';
                        $newMessage    = "RMS file has been imported." . "\n" . $updated_count . " rows created successfully " . "\n" . $error_count . " rows could not be imported ";
                        $message       = ['message' => $newMessage];
                        Notification::send($user, new RMSConsumed($message, $title));
                    }

                } catch (\Exception $e) {
                    // transferring file with error to error folder //
                    $message      = ['message' => $e->getMessage() . '  @Line:' . $e->getLine()];
                    $title        = 'Error! Get in guys, we goin diggin';
                    $pathCopyFrom = $input_file;
                    $pathAr       = explode('/', $pathCopyFrom);
                    $fileName     = end($pathAr);
                    $pathCopyTo   = '/sit/error/' . $fileName;
                    Storage::disk('s3rms')->move($pathCopyFrom, $pathCopyTo);

                    // creating log for error file //
                    DB::table('temp')->insert([
                    	'cause' => $e->getMessage(), 
                    	'reason' => '',
                    	'file'	=> $fileName
                    ]);

                    // deleting source file and sending error notification //
                    $deletepath = public_path('/uploads/' . $destinationPath);
                    \File::delete($deletepath);
                    Notification::send($user, new RMSConsumed($message, $title));

                    return $e->getMessage();
                }
            } 
            //endif of file checking
        }

    }

}
